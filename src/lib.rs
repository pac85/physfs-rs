//!This crate aims at safely wrapping PhysicsFs, a library that implements a virtual file system
//!mainly for moddable videogames. Still WIP.

use physfs_sys;
use std::fmt;
use std::sync::Mutex;

use lazy_static::lazy_static;

lazy_static! {
    static ref INSTANCED: Mutex<bool> = Mutex::new(false);
}

///Represents an error returned by PhysicsFs
pub struct PhysFsError {
    code: physfs_sys::PHYSFS_ErrorCode,
}

impl PhysFsError {
    pub(crate) fn new(code: physfs_sys::PHYSFS_ErrorCode) -> Self {
        Self { code }
    }

    pub fn get_text(&self) -> String {
        let v = unsafe {
            std::ffi::CStr::from_ptr(physfs_sys::PHYSFS_getErrorByCode(self.code))
                .to_bytes()
                .to_vec()
        };

        String::from_utf8(v).unwrap()
    }
}

impl fmt::Debug for PhysFsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("PhysFsError")
            .field("code", &self.code)
            .field("text", &self.get_text())
            .finish()
    }
}

impl fmt::Display for PhysFsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.get_text())
    }
}

use std::error::Error;

impl Error for PhysFsError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

///A handle to a file opend through PhysicsFs
pub struct PhysFsHandle {
    handle: *mut physfs_sys::PHYSFS_File,
}

impl PhysFsHandle {
    pub(crate) fn new(handle: *mut physfs_sys::PHYSFS_File) -> Self {
        Self { handle }
    }

    pub fn file_length(&self) -> u64 {
        unsafe { physfs_sys::PHYSFS_fileLength(self.handle) as u64 }
    }

    pub fn read_to_vec(&mut self) -> std::io::Result<Vec<u8>> {
        let len = self.file_length() as usize;
        let mut v = Vec::with_capacity(len);
        v.resize(len, 0);
        std::io::Read::read(self, v.as_mut_slice())?;
        Ok(v)
    }

    pub fn close(self) {}
}

//PhysFs is supposed to manage concurrent access on its own
//through a global mutex so it should be thread safe
unsafe impl Send for PhysFsHandle {}
unsafe impl Sync for PhysFsHandle {}

impl std::io::Read for PhysFsHandle {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let read_bytes = unsafe {
            physfs_sys::PHYSFS_readBytes(
                self.handle,
                buf.as_mut_ptr() as *mut std::ffi::c_void,
                buf.len() as u64,
            )
        };
        if read_bytes == -1 {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                get_last_error().err().unwrap(),
            ))
        } else {
            Ok(read_bytes as usize)
        }
    }
}

impl std::io::Write for PhysFsHandle {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        Ok(unsafe {
            physfs_sys::PHYSFS_writeBytes(
                self.handle,
                buf.as_ptr() as *const std::ffi::c_void,
                buf.len() as u64,
            ) as usize
        })
    }

    fn flush(&mut self) -> std::io::Result<()> {
        unsafe {
            let ret = physfs_sys::PHYSFS_flush(self.handle);
            if ret != 0 {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    get_last_error().err().unwrap(),
                ));
            }
        }
        Ok(())
    }
}

use std::io::SeekFrom;
impl std::io::Seek for PhysFsHandle {
    fn seek(&mut self, pos: SeekFrom) -> std::io::Result<u64> {
        match pos {
            SeekFrom::Start(pos) => unsafe {
                let ret = physfs_sys::PHYSFS_seek(self.handle, pos);
                if ret == 0 {
                    Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        get_last_error().err().unwrap(),
                    ))
                } else {
                    Ok(physfs_sys::PHYSFS_tell(self.handle) as u64)
                }
            },
            SeekFrom::Current(pos) => unsafe {
                let cpos = physfs_sys::PHYSFS_tell(self.handle);
                self.seek(SeekFrom::Start((cpos + pos) as u64))
            },
            SeekFrom::End(pos) => unsafe {
                let cpos = physfs_sys::PHYSFS_fileLength(self.handle);
                self.seek(SeekFrom::Start((cpos + pos) as u64))
            },
        }
    }
}

impl Drop for PhysFsHandle {
    fn drop(&mut self) {
        unsafe {
            let _res = physfs_sys::PHYSFS_close(self.handle);
            //TODO check error code?
        }
    }
}

fn get_last_error() -> Result<(), PhysFsError> {
    Err(unsafe { PhysFsError::new(physfs_sys::PHYSFS_getLastErrorCode()) })
}

///This struct doesn't store any state, his sole purpose is to ensure that all PhysicsFs calls
///are done after calling his init function. Only ine instance of it can exist at any given time.
///This is because PhysicsFs has a global state so this struct makes sure rust borrowing rules are
///enforced
pub struct PhysFs {}

fn create_ctsr(s: impl AsRef<str>) -> std::ffi::CString {
    std::ffi::CString::new(s.as_ref().as_bytes()).unwrap()
}

macro_rules! to_cstr {
    ($s:expr) => {
        create_ctsr($s).as_bytes().as_ptr() as *const i8
    };
}

impl PhysFs {
    fn new() -> Self {
        unsafe {
            physfs_sys::PHYSFS_init(std::ptr::null());
        }

        Self {}
    }

    ///When calling this method for the first time an instance of Self will be returned, if called
    ///again it will return None ensuring that no more than one instance exists
    pub fn get() -> Option<Self> {
        let mut instanced_lock = INSTANCED.lock().unwrap();
        let ret = if !*instanced_lock { Some(Self::new()) } else { None };
        *instanced_lock = true;

        ret
    }

    ///Mounts `dir` to `mount_point`.
    ///`dir` can either be a path to an archive of a supported format or to a directory
    pub fn mount(
        &mut self,
        dir: impl AsRef<str>,
        mount_point: impl AsRef<str>,
        append: bool,
    ) -> Result<(), PhysFsError> {
        unsafe {
            if 0 == physfs_sys::PHYSFS_mount(
                to_cstr!(dir),
                to_cstr!(mount_point),
                if append { 1 } else { 0 },
            ) {
                get_last_error()?;
            }
        }

        Ok(())
    }

    unsafe fn make_handle(
        &self,
        handle: *mut physfs_sys::PHYSFS_File,
    ) -> Result<PhysFsHandle, PhysFsError> {
        if handle == std::ptr::null_mut() {
            get_last_error()?;
        }

        Ok(PhysFsHandle::new(handle))
    }

    ///Open file inside virtual fs for reading
    pub fn open_read(&self, path: impl AsRef<str>) -> Result<PhysFsHandle, PhysFsError> {
        unsafe { self.make_handle(physfs_sys::PHYSFS_openRead(to_cstr!(path))) }
    }

    ///Open file inside virtual fs for writing. If the file already exists it will be truncated
    pub fn open_write(&self, path: impl AsRef<str>) -> Result<PhysFsHandle, PhysFsError> {
        unsafe { self.make_handle(physfs_sys::PHYSFS_openWrite(to_cstr!(path))) }
    }

    ///Open file inside virtual fs for writing. If the file already exists new writes will be
    ///appended to the existing contents
    pub fn open_append(&self, path: impl AsRef<str>) -> Result<PhysFsHandle, PhysFsError> {
        unsafe { self.make_handle(physfs_sys::PHYSFS_openAppend(to_cstr!(path))) }
    }

    ///Enumerates the files in a given searchpath directory. None if erros occur
    pub fn enumerate_files(&self, path: impl AsRef<str>) -> Option<Vec<String>> {
        let mut list = unsafe {physfs_sys::PHYSFS_enumerateFiles(to_cstr!(path))};
        if list == std::ptr::null_mut() {
            return None;
        }

        let mut res = vec![];
        while unsafe{*list} != std::ptr::null_mut() {
            unsafe {
                let filename = std::ffi::CStr::from_ptr(*list).to_str().unwrap();
                res.push(filename.to_owned());
            }
            list = ((list as usize) + std::mem::size_of_val(&list)) as _;
        }

        return Some(res);
    }

    pub fn is_directory(&self, path: impl AsRef<str>) -> bool {
        unsafe{
            physfs_sys::PHYSFS_isDirectory(to_cstr!(path)) == 1
        }
    }
}

impl Drop for PhysFs {
    fn drop(&mut self) {
        let mut instanced_lock = INSTANCED.lock().unwrap();
        *instanced_lock = false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::*;

    //tests seem to run in parallel threads, wait for the instance to be available
    fn wait_instance() -> PhysFs {
        let mut instance = None;
        while instance.is_none() {
            instance = PhysFs::get();
        }
        instance.unwrap()
    }

    const TEST_TEXT: &'static str = "testing\n";
    #[test]
    fn it_works() {
        let mut fs = wait_instance();
        fs.mount("alol.zip", "", true).err().unwrap(); //muest give none
        fs.mount("test.zip", "", true).unwrap();
        fs.mount("./", "", true).unwrap();
        let mut handle = fs.open_read("test").unwrap();

        assert_eq!(handle.file_length(), 8);
        let mut buf = [0; 8];
        handle.read(&mut buf).unwrap();
        assert_eq!(String::from_utf8_lossy(&buf), TEST_TEXT);
        handle.close();

        let mut handle = fs.open_read("test").unwrap();
        assert_eq!(handle.read_to_vec().unwrap(), TEST_TEXT.as_bytes().to_vec());

        assert!(PhysFs::get().is_none());
    }

    #[test]
    fn files_enumeration() {
        let mut fs = wait_instance();
        fs.mount("test.zip", "", true).unwrap();

        let files = fs.enumerate_files("/").unwrap();
        assert_eq!(vec!["test"], files);
        assert!(!fs.is_directory("test"));
        fs.mount("./", "/", true);
        assert!(fs.is_directory("src"));
    }
}
